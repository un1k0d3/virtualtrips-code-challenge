import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import './App.css';
import { ILocation } from './types/location.model';

function App() {

  const [value, setValue] = useState<string>('');
  const [locations, setLocations] = useState<ILocation[]>([]);

  useEffect(() => {
    setLocations([]);
    if (value) {
      const cancelTokenSource = Axios.CancelToken.source();

      Axios.get(`${process.env.REACT_APP_API_URL}/locations`, {
        params: {
          q: value
        },
        cancelToken: cancelTokenSource.token,
      }).then(({data}) => {
        setLocations(data);
      }).catch((error) => {
        console.error(error);
      })

      return () => {
        cancelTokenSource.cancel();
      }
    }
  }, [value])
  
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value || '');
  }

  
  return (
    <div className="App">
      <header className="App-header">
        <h1>Code challenge</h1>
        <form onSubmit={e => e.preventDefault()}>
          <input type="text" name="location" value={value} onChange={onChange} />
        </form>
        <ul style={{ textAlign: 'left' }}>
          {locations?.map((location) => {
            const { id, name, latitude, longitude } = location;
            return (
              <li key={`location_${id}`}>
                {name} ({latitude}, {longitude})
              </li>
            )
          })}
        </ul>
      </header>
    </div>
  );
}

export default App;
