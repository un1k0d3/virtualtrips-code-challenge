const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

chai.use(chaiHttp);
chai.should();

describe('Locations', () => {
  describe('/GET locations', () => {
      it('it should get an error when no q query params', (done) => {
        chai.request(server)
            .get('/locations')
            .end((err, res) => {
              res.should.have.status(400);
              done();
            });
      });
      it('it should get an array of locations with q query params', (done) => {
        chai.request(server)
            .get('/locations?q=Hastin')
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('array');
              done();
            });
      });
  });

});