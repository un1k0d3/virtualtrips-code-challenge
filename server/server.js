require('dotenv').config();
const sqlite3 = require('sqlite3');
const db = new sqlite3.Database('GB.db');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
  next();
})

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(process.env.HTTP_PORT, () => {
  console.log("Server is listening on port " + process.env.HTTP_PORT);
});

app.get("/locations", (req, res) => {
  const { q } = req.query;
  if (q) {
    db.all(`SELECT id, name, latitude, longitude FROM locations WHERE name LIKE '%${q}%' ORDER BY abs(length('${q}') - length(name)) LIMIT 5`, (err, data) => {
      if (err) {
        res.status(400).json({ "error": err.message });
        return;
      }
      res.status(200).json(data);
    });
  } else {
    res.status(400).json({ "error": `Wrong query format, use /locations?q=query` });
  }
});

module.exports = app;