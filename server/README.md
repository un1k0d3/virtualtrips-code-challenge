# Geo search API server

NodeJS version : v10.16.3

## Start the server

```sh
$ npm start
```

## Start the tests

```sh
$ npm test
```