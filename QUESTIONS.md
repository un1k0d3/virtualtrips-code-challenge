# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Output:
2
1

1 will be displayed after 2 because of the event loop (asynchronous code runs after all the synchronous code is done executing)



Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
Output:
10
9
8
7
6
5
4
3
2
1
0

Because of the recursivity and the call stack, the first execution of the console.log will be from the last foo() invoked.


Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

If we invoke foo(0), d will be replaced by 5. It's better to test if it's null or undefined.

Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Output: 3
Because of closure, bar(2) will add 2 to the variable a, already set at 1 from foo(1);

Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

The function would be used like this : 

```js
    double(4, (result) => {
      console.log(result);
    });
```
Where the output will be 8